/**
 *  1. get random num between 1 and 78 (example 4)
 *  2. get request to API where num is 3 http://94.198.40.18:3000/api/cards?filter={"where":{"num":"3"}}
 *  3. receive picture - part of the pic name
 *
 *
 *
 * */
class Shirt extends LayoutsSite {
  constructor() {
    super();
    console.log("shirt start");
    //this.getVarsFromUrl();
    this.url = this.url + 1;
    this.getJSON();
    this.reverse = 0;
    this.arcana = "All";
    this.usedCards = [];
  }

  getRandNum() {
    let randNum = Math.floor(Math.random() * 78) + 1;
    let randNumForReverse = Math.floor(Math.random() * 78) + 1;
    if (randNumForReverse % 2) {
      this.reverse = 1;
    }

    if (this.usedCards.indexOf(randNum) === -1 || randNum !== 'undefined') {
      this.usedCards.push(randNum);
      return randNum;
    } else {
      this.getRandNum();
    }
  }

  getRandNumMajor() {
    let randNum = Math.floor(Math.random() * 22) + 1;
    let randNumForReverse = Math.floor(Math.random() * 78) + 1;
    if (randNumForReverse % 2) {
      this.reverse = 1;
    }
    if (this.usedCards.indexOf(randNum) === -1 || randNum !== 'undefined') {
      this.usedCards.push(randNum);
      return randNum;
    } else {
      this.getRandNum();
    }
  }

  /*getVarsFromUrl() {
    var $_GET = [];
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (a, name, value) {
      $_GET[name] = value;
    });
    this.url = this.url + $_GET['layout'];
  }*/

  createHTML() {
    let that = this;
    this.callback = function (err, data) {
      if (err !== null) {
        console.log(err);
        console.log(data);
      } else {
        that.data = data.data;
        that.arcana = data.data.attributes.arcana;
        let strDescription = ``;
        let str = `<div class="text-center col-lg-12"><h5><b><div id="layoutName"> ${data.data.attributes.layoutName} </div> (карт: ${data.data.attributes.cards}, аркан: ${data.data.attributes.arcana}) </b><h5></div>`;
        let id = 0;
        let weekArr = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье", "Общий расклад"];
        let divinationSituation = ["Ситуация", "Причина", "Исход"];
        let relationships = ["Настоящее", "Прошлое", "Ваш партнёр", "События", "Мысли"];
        while (id < data.data.attributes.cards) {
          str += `<div class="col-lg-3 mb-3"><p style="text-align: center;"><img id="shirtImg_${id}" src="img/stuff/shirtcard.jpg" onclick="createShirts.revertCard('${id}');" width='200px' height='350px'/>`;
          if (data.data.attributes.cards == 7 || data.data.attributes.cards == 8) {
            str += `<span id="weekDay_${id}" class="badge badge-dark" style="width:200px; text-align: center; font-size:18px; margin:2px ">${weekArr[id]}</span>`;
          }
          if (data.data.attributes.cards == 3) {
            str += `<span id="weekDay_${id}" class="badge badge-dark" style="width:200px; text-align: center; font-size:18px; margin:2px ">${divinationSituation[id]}</span>`;
          }
          if (data.data.attributes.cards == 5) {
            str += `<span id="weekDay_${id}" class="badge badge-dark" style="width:200px; text-align: center; font-size:18px; margin:2px ">${relationships[id]}</span>`;
          }
          str += `</p></div>`;
          strDescription += `<a id="anchor_${id}"></a><div class="col-lg-12 mb-3" id="cardsInfo_${id}"></div>`;
          id++;
        }
        document.getElementById("cardShirts").innerHTML = str;
        document.getElementById("cardDescription").innerHTML = strDescription;
      }
    }
  }

  revertCard(id) {
    let that = this;
    if (this.arcana == "Major") {
      this.url = this.siteUrl + '/api/cards?populate=picture';
    } else {
      this.url = this.siteUrl + '/api/cards?populate=picture';
    }
    this.callback = function (err, data) {
      if (err) {
        console.log(err);
        return;
      }
      let infoStr = ``;
      if ($("#weekDay_" + id).length) {
        infoStr = '<span id="weekDay_${id}" class="badge badge-dark" style="width:200px; text-align: center; font-size:18px; margin:2px ">' + $("#weekDay_" + id).text() + '</span>';
      }
      if (!that.reverse) {
        console.log("picture !that.reverse")
        console.log("data.data[0] !that.reverse", data.data[0])
        console.log(data.data[0].attributes.picture.data.attributes.url)
        //console.log("picture", that.data[0].picture.data.attributes.name, data.data[0].attributes.picture.data.attributes.url)
        document.getElementById("shirtImg_" + id).src = this.siteUrl + data.data[0].attributes.picture.data.attributes.url;
        // document.getElementById("shirtImg_" + id).onclick = "document.location+='#newTest'; return false;";
        // $("#shirtImg_" + id).attr("onclick", "document.location+='#newTest'; return false;")
        infoStr += `<div class="col-lg-12 mb-3" style="border: double 3px; padding: 10px;"><img style="float: left; margin: 5px 10px 1px 0px" width='100px' height='200px' src="${this.siteUrl + data.data[0].attributes.picture.data.attributes.url}" /> <b>Карта:</b> ${data.data[0].attributes.name} </br></br><b>Описание:</b> ${data.data[0].attributes.text}  </br></br><b>Значение:</b> ${data.data[0].attributes.meaning}</br></div>`;
        console.log(data.data[0].attributes.meaning);
        document.getElementById("cardsInfo_" + id).innerHTML = infoStr;
      } else {
        console.log("picture")
        console.log("data.data[0]", data.data[0])
        console.log("picture", data.data[0].attributes.picture.data.attributes.url, data.data[0].attributes.picture.data.attributes.url)
        document.getElementById("shirtImg_" + id).src = data.data[0].attributes.picture.data.attributes.url;
        infoStr += `<div class="col-lg-12 mb-3" style="border: double 3px; padding: 10px;"><img style="float: left; margin: 5px 10px 1px 0px;" width='100px' height='200px' src="${this.siteUrl + data.data[0].attributes.picture.data.attributes.url}" /> <b>Карта:</b> ${data.data[0].attributes.name} <b>(Перевёрнута)</b> </br></br><b>Описание:</b> ${data.data[0].attributes.text}  </br></br><b>Значение:</b> ${data.data[0].attributes.reverse}</br></div>`;
        console.log(data.data[0].attributes.reverse);
        document.getElementById("cardsInfo_" + id).innerHTML = infoStr;
      }
      // document.getElementById("shirtImg_" + id).onclick = () => false;
      document.getElementById("shirtImg_" + id).onclick = function () {
        if (window.location.toString().indexOf('#') != -1) {
          var location = window.location.toString().split('#');
          document.location = location[0] + '#anchor_' + id;
        } else {
          console.log("else document.location ");
          document.location += '#anchor_' + id;
        }
        return false;
      };
      that.reverse = 0;
      let weekArr = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"];
      if (getCookie("debug")) {
        if ($("#debugtest").text() == "") {
          $("#debugtest").text("Как сочетаются друг с другом эти карты таро? В раскладе " + $("#layoutName").text())
        }
        for (let i = 0; i < weekArr.length; i++) {
          if ($("#weekDay_" + id).text() == weekArr[i]) {
            $("#debugtest").html($("#debugtest").text() + "Карта " + data.data[0].attributes.name + " выпавшая на день недели " + $("#weekDay_" + id).text() + ", ");
          }
        }
        //$("#debugtest").html($("#debugtest").text() + "Карта " + data[0].name + ", ");
      }
      console.log($("#layoutName").text())
    }
    this.getJSON();
  }

}

