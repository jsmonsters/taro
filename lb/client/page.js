class Page extends BaseSite {
  constructor() {
    super();
    this.url = this.siteUrl + "/api/pages/";
  }

  createHTML() {
    let that = this;
    this.callback = function (err, data) {
      if (err !== null) {
        console.log(err);
      } else {
        that.data = data;
        let $_GET = [];
        window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (a, name, value) {
          $_GET[name] = value;
        });
        let str = ``;
        let randomPage = ``;
        let lastPage = ``;
        let j = Math.floor(Math.random() * data.length);
        for (let i = 0; i < data.length; i++) {
          $('meta[property="og:description"]').attr('content',data[i].description);
          if ($_GET['id'] === data[i].id) {
            str = `
<div class='media position-relative mb-3' style="border: double; padding: 6px;">
    <div class='media-body'>
        <span style="font-weight: 900;">${data[i].title}</span>
        <span style="position: relative; float: right;">Автор: ${data[i].author}</span>
        <span style="position: relative; float: left; padding: 8px;"><img style="width: 250px;"  src="${data[i].picture}" /></span>
        <p style="padding: 3px;">
        <span style="font-size: 16px;">${data[i].text}</span>
        </p>
        <span style="font: small-caption;">Теги: ${data[i].tags}</span>
    </div>
</div>`
          }
        }
        lastPage += `
<div class='media position-relative mb-3' style="border: double; padding: 6px;">
  <div class='media-body'>
    <img src='${data[data.length-1].picture ? data[data.length-1].picture : "/img/stuff/nopicture.jpg"}' width='200px' height='200px' style='margin: 7px; float: left;'
                            alt='${data[data.length-1].title}'>
<a class='stretched-link' style="color: #0a0a0a; font-size: 20px; margin-top: 12px;" href='page.html?id=${data[data.length-1].id}'>
            <b>${data[data.length-1].title} </b>
        </a><br/><span id="text" style="display: block; overflow: hidden; text-overflow: ellipsis; max-height: 200px; background: linear-gradient(#ffffff, #fdfdfd, #2a2a2a);">${data[data.length-1].text}</span>
        <span style="font: small-caption;">Теги: ${data[data.length-1].tags}</span>
  </div>
</div>`
        if (data[j] === data[data.length-1]) {
          j--;
        }
        randomPage += `
<div class='media position-relative mb-3' style="border: double; padding: 6px;">
  <div class='media-body'>
    <img src='${data[j].picture ? data[j].picture : "/img/stuff/nopicture.jpg"}' width='200px' height='200px' style='margin: 7px; float: left;'
                            alt='${data[j].title}'>
<a class='stretched-link' style="color: #0a0a0a; font-size: 20px; margin-top: 12px;" href='page.html?id=${data[j].id}'>
            <b>${data[j].title} </b>
        </a><br/><span id="text" style="display: block; overflow: hidden; text-overflow: ellipsis; max-height: 200px; background: linear-gradient(#ffffff, #fdfdfd, #2a2a2a);">${data[j].text}</span>
        <span style="font: small-caption;">Теги: ${data[j].tags}</span>
  </div>
</div>`
        document.getElementById("thisPage").innerHTML = str;
        document.getElementById("randomPage").innerHTML = randomPage;
        document.getElementById("lastPage").innerHTML = lastPage;
      }
    }
    this.getJSON();
  }

}
