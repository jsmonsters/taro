class LayoutsSite extends BaseSite {

  constructor() {
    super();
    this.url =  this.siteUrl + "/api/layouts/";
    this.data = [];
  }

  createRandomDiv() {
    let that = this;
    this.callback = function (err, data) {
      if (err !== null) {
        console.log(err);
      } else {
        that.data = data;
        console.log(data);
        let i = Math.floor(Math.random() * data.length);
        let str = ``;
        str = `<div class='media position-relative mb-3' style="border: double; padding: 6px;">

  <div class='media-body'>

    <img src='img/divinations/divination_${i + 1}.jpg' width='200px' height='200px' style='margin: 7px; float: left;'
                            alt='${data.data[i].attributes.layoutName}'>
<a class='stretched-link' style="color: #0a0a0a; font-size: 20px; margin-top: 12px;" href='near-future.html?layout=${data.data[i].attributes.id}'>
            <b>${data.data[i].attributes.layoutName} (карт: ${data.data[i].attributes.cards}, аркан: ${data.data[i].attributes.arcana}) </b>
        </a><br/><span style="font-size: 16px;">${data.data[i].attributes.layoutDescription}</span>
  </div>
</div>
</div>
</div>`;
        document.getElementById("thisLayout").innerHTML = str;

      }
    }
    //reduce the number of requests to the server
    if(!this.data.length){
      this.getJSON();
    }else {
      this.callback(null, this.data);
    }
  }

  createHTML() {
    console.log("init");
    let that = this;
    this.callback = function (err, data) {
      if (err !== null) {
        console.log(err);
      } else {
        that.data = data;
        console.log(data.data[0].attributes.layoutName);
        console.log("data",data);
        let str = ``;

        for (let i = 0; i < data.data.length; i++) {

          console.log(data.data[0].attributes.layoutName);
          console.log("data.data[i].attributes.id",data.data[i].id);
          str += `
<div class='media position-relative mb-3' style="border: double; padding: 6px;">

  <div class='media-body'>

    <img src='img/divinations/divination_${i + 1}.jpg' width='200px' height='200px' style='margin: 7px; float: left;'
                            alt='${data.data[i].attributes.layoutName}'>
<a class='stretched-link' style="color: #0a0a0a; font-size: 20px; margin-top: 12px;" href='near-future.html?layout=${data.data[i].id}'>
            <b>${data.data[i].attributes.layoutName} (карт: ${data.data[i].attributes.cards}, аркан: ${data.data[i].attributes.arcana}) </b>
        </a><br/><span style="font-size: 16px;">${data.data[i].attributes.layoutDescription}</span>
  </div>
</div>`
        }
        str += `</div>
            </div>`;
        console.log(str);
        document.getElementById("thisLayout").innerHTML = str;
      }
    }
    this.getJSON();
  }
}
