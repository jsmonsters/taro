class BaseSite {
  constructor() {
    this.siteUrl = "http://localhost:1337";
    this.headerAndFooter();
  }

  getJSON() {
    var that = this;
    var xhr = new XMLHttpRequest();
    console.log(this.url)
    xhr.open('GET', this.url, true);
    xhr.responseType = 'json';
    xhr.onload = function () {
      var status = xhr.status;
      if (status === 200) {
        that.callback(null, xhr.response, that);
      } else {
        that.callback(status, xhr.response, that);
      }
    };
    xhr.send();
  };

  headerAndFooter() {
    let header = "<header class=\"d-flex flex-wrap justify-content-between align-items-center p-1\" style='position: absolute; top: 0; width: 100%; background-color: #000000'>\n" +
      "      <ul class=\"nav \">\n" +
      "        <li class=\"nav-item\"><a style='color: #fff; text-transform: full-width; font-size: 18px' href=\"index.html\">" +
      "          <img src='img/stuff/cards.jpg'/></a></li>\n" +
      "      </ul>\n" +
      "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\n" +
      "        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\n" +
      "                aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n" +
      "          <span class=\"navbar-toggler-icon\"></span>\n" +
      "        </button>\n" +
      "        <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\n" +
      "          <ul class=\"navbar-nav\">\n" +
      "            <li class='nav-item '> " +
      "              <a class='nav-link' href='index.html'>Расклады</a>" +
      "            </li>\n" +
      "            <li class=\"nav-item\">\n" +
      "              <a class=\"nav-link\" href=\"articles.html\">Статьи</a>\n" +
      "            </li>\n" +
      "          </ul>\n" +
      "        </div>\n" +
      "      </nav>  " +
      "     </header>" +
      "     <br/>\n";
    let footer = "<footer class=\"text-center text-white\" style='position: absolute; bottom: 0; width: 100%; background-color: #cfcfcf'>" +
      "  <div class=\"text-center \" style=\"background-color: rgba(0, 0, 0, 0.2);\">\n" +
      "    © 2022 - 2023 Copyright:\n" +
      "    <a class=\"text-white\" href=\"https://t.me/Divination_TarotBot\">@Divination_TarotBot</a>\n" +
      "    <a class=\"btn m-1 p-0 pl-1 pr-1\" style='color: white' \n" +
      "        href=\"https://t.me/Divination_TarotBot\"\n" +
      "        role=\"button\"\n><i class=\"fab fa-telegram-plane\"></i>\n" +
      "      </a>\n" +
      "  </div>\n" +
      "</footer>";
    document.getElementById("header").innerHTML = header;
    document.getElementById("footer").innerHTML = footer;
  }
}
