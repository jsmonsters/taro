class ArticlesSite extends BaseSite {
  constructor() {
    super();
    this.url = this.siteUrl + "/api/pages?populate=picture";
  }

  createHTML() {
    console.log("init");
    let that = this;
    this.callback = function (err, data) {
      if (err !== null) {
        console.log(err);
      } else {
        that.data = data.data.reverse();
      console.log(that.data)
        let str = ``;
        let tagsArr = [];
        let tagsStr = "Категории: ";
        for (let i = 0; i < that.data.length; i++) {
          console.log(that.data[i].attributes.tags)
          if(tagsArr.indexOf("&nbsp;<a href='#'>" + that.data[i].attributes.tags + "</a>") === -1){
            tagsArr.push("&nbsp;<a href='#'>" + that.data[i].attributes.tags + "</a>,");
          }

          console.log("----------------------------------- tagsArr");
          console.log(tagsArr);
          console.log("----------------------------------- str");
          console.log(str);

          if (that.data[i].attributes.active == true) {
            str += `
<div class='media position-relative mb-3' style="border: double; padding: 6px;">

  <div class='media-body'>

    <img src='${this.siteUrl + that.data[i].attributes.picture.data.attributes.url ? this.siteUrl + that.data[i].attributes.picture.data.attributes.url : "/img/stuff/nopicture.jpg"}' width='200px' height='200px' style='margin: 7px; float: left;'
                            alt='${that.data[i].attributes.title}'>
<a style="color: #0a0a0a; font-size: 20px; margin-top: 12px;" href='page.html?id=${that.data.id}'>
            <b>${that.data[i].attributes.title} </b>
        </a><br/><span id="text" style="display: block; overflow: hidden; text-overflow: ellipsis; max-height: 200px; background: linear-gradient(#ffffff, #fdfdfd, #2a2a2a);">${that.data[i].attributes.text}</span>
        <span style="font: small-caption;">Категории: <a href="#">${that.data[i].attributes.tags}</a></span>
  </div>
</div>`
          }
          str += `</div>
            </div>`;

          /*tagsArr[i].addEventListener('click', function() {
            data.forEach(function(item){
              console.log(item)
              if(that.data.tags.indexOf(tagsArr) == -1){
                item.style.display = "none";
                return;
              }
              item.style.display = "block";
            })
          })*/

        }

        for (let i = 0; i < tagsArr.length; i++) {
          tagsStr += tagsArr[i];
        }
        document.getElementById("thisArticle").innerHTML = str;
        document.getElementById("thisTags").innerHTML = tagsStr;
      }
    }
    this.getJSON();
  }

}
