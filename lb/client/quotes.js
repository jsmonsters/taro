class Quote extends BaseSite {
  constructor() {
    super();
    this.url = this.siteUrl + "/api/quotes?populate=picture";
  }

  showQuotes() {
    let that = this;
    this.callback = function (err, data) {
      if (err !== null) {
        console.log(err);
      } else {
        that.data = data.data;
        console.log(that.data);
        let i = 0; /* Math.floor(Math.random() * data.length);*/
        console.log(that.data[0].attributes.picture);
        console.log(that.data[0].attributes.picture.data.attributes.url);

        let str = ``;
          str = `
<div class='media position-relative mb-3' style="border: double; padding: 6px;">
    <div class='media-body'>
    <span style="position: relative; float: right;"><i>${that.data[i].attributes.author}</i></span>
        <p style="padding: 3px;">
        <span style="position: relative; float: left; padding: 8px;"><img style="width: 120px" src="${this.siteUrl + that.data[0].attributes.picture.data.attributes.url}" /></span>
    <span style="font-size: 16px;"><b>${that.data[i].attributes.text}</b></span>
    </div>
</div>`
      document.getElementById("quotesPage").innerHTML = str;
     /*     document.getElementById("quotesArticles").innerHTML = str;*/
      }
    }
    this.getJSON();
  }

}
