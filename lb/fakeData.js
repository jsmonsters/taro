const {MongoClient} = require('mongodb');
// or as an es module:
// import { MongoClient } from 'mongodb'

// Connection URL
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);

// Database Name
const dbName = 'taro';

async function main() {
  // Use connect method to connect to the server
  await client.connect();
  console.log('Connected successfully to server');
  const db = client.db(dbName);
  const collection = db.collection('cards');


  let dataArr = [];
  let i = 0;
  let c = 1;
  while (i <= 156) {
    if (i === 78) {
      c  = 0;
    }
    dataArr.push({
      name: 'Record ' + i + ' <b>The Fool / Дурак, Безумец</b>',
      text: 'Здравствуй, зеро. Шарик вашей судьбы попал в эту лунку на игорном столе – значит, готовьтесь к переменам. Ноль, зеро – суть чистый лист, новый жизненный цикл и шаг в неизвестность. Что там, за перевалом? Вот он, шут на Старшем Аркане: веселый дурак, который не видит, что следующий его шаг – это шаг в пропасть. Что будет, когда ноги его не почувствуют под собой опоры? Полет или падение? У вас появились новые возможности – вот он, мир, как на ладони, только не будьте слишком легкомысленны. Слишком много неожиданных поворотов сулит этот путь, так что проявите осторожность и расчетливость. Шагая в пропасть, дурак не ведает что творит, и оттого синоним карты – абсолютное безрассудство. Так что неожиданность, подстерегающая вас на новом пути может быть неприятной. Откройте глаза и оглянитесь! – говорит Шут. Возможно, вы слишком расточительны или упрямы? Может быть, не хотите замечать очевидного? Тарологи уверены, что Шут выпадает только тем, кто заведомо ведет себя глупо или сумасбродно, только нипочем не желает признаться в этом.',
      meaning: 'Вы жаждете новых ощущений и необычных ощущений, и жажда эта толкает вас на необдуманные поступки и скоропалительные решения. Это выглядит так, будто вы вдавили педаль газа в пол и наслаждаетесь мельканием картинок за окном, свистом ветра. Сбросьте же скорость! А то и вовсе остановитесь на обочине и осмотритесь. Каждое событие сейчас имеет для вас огромное значение – вокруг сплошные знаки, обратите на них свое внимание, советует Таро.',
      url: 'https://horo.mail.ru/img/horo/tarot/card/180/' + c + '.png'
    });
    c++;
    i++;
  }
  const insertResult = await collection.insertMany(dataArr);

  console.log('Inserted documents =>', insertResult);

  /*  const findResult = await collection.find({}).toArray();

    console.log("###########################category##################################");
    console.log('Found documents =>', findResult);
    console.log("###########################/category##################################");*/

  // the following code examples can be pasted here...

  return 'done.';
}

main()
  .then(console.log)
  .catch(console.error)
  .finally(() => client.close());
