/**
 *  1. downloads all pics using Promises https://stackoverflow.com/questions/39179103/saving-multiple-files-using-promise
 *  2. setInterval
 *
 * */
var fs = require('fs'), request = require('request');
var Jimp = require('jimp');
var md4 = require('js-md4');
var i = 1;
var sole = "SomeText!"


var download = function (uri, filename, callback) {
  request.head(uri, function (err, res, body) {
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);
    //https://stackoverflow.com/questions/39179103/saving-multiple-files-using-promise
    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

const timeValue = setInterval(() => {

  if (i <= 78) {
    var hashImg = md4(sole + i);

    const path = 'c:\\Users\\Administrator\\WebstormProjects\\taro\\lb\\client\\img\\cards\\' + hashImg + '.png'
    try {
      if (fs.existsSync(path)) {
        i++;
        hashImg = md4(sole + i);
      }
      download('https://horo.mail.ru/img/horo/tarot/card/180/' + i + '.png', 'c:\\Users\\Administrator\\WebstormProjects\\taro\\lb\\client\\img\\cards\\' + hashImg + '.png', function (i) {
        console.log(i + ' done ' + hashImg);
      })
    } catch (err) {
      console.error(err)
    }
  } else {
    clearInterval(timeValue);
  }
}, 5000);

