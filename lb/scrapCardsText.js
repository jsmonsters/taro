const puppeteer = require('puppeteer');
/**
 * https://devdocs.io/puppeteer/
 */

// or as an es module:
const {MongoClient} = require('mongodb');
const fs = require("fs");
const md4 = require("js-md4");

// Connection URL
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);

const dbName = 'taro';
const pagesNum = 78;

let arcana = "Major";
let suit = "";

let i = 1;
let dataArr = [];
var sole = "SomeText!"

async function delay(ms) {
  // return await for better async stack trace support in case of errors.
  return await new Promise(resolve => setTimeout(resolve, ms));
}

async function getData() {

  await client.connect();
  console.log('Connected successfully to server');
  const db = client.db(dbName);

  const collection = db.collection('cards');
  const browser = await puppeteer.launch();
  const page = await browser.newPage();


  while (i <= pagesNum) {
    if (i <= pagesNum) {
      let vgmUrl = 'https://horo.mail.ru/divination/tarot/card/' + i + '/';
      console.log(">>>>>>>>>>>>>>>>>>>>>>>>>> vgmUrl " + vgmUrl);

      try {
        await page.goto(vgmUrl);
        await page.waitForSelector('.text')
        const elHandleArray = await page.$$('.text')
        let titleOnCard = await page.evaluate(el => el.textContent, elHandleArray[0])
        console.log("----------------------------------------> titleOnCard " + titleOnCard);

        let textValue = await page.evaluate(el => el.textContent, elHandleArray[1])
        console.log("----------------------------------------> textValue " + textValue);

        const elHandleMeanArray = await page.$$('.article__item')

        let meaningValue = await page.evaluate(el => el.textContent, elHandleMeanArray[0])
        console.log("----------------------------------------> titleOnCard " + meaningValue);

        let reverseMeaningValue = await page.evaluate(el => el.textContent, elHandleMeanArray[1])
        console.log("----------------------------------------> textValue " + reverseMeaningValue);

        if(i > 22) {
          arcana = 'Minor';
          if(titleOnCard.indexOf("Жезл") + 1){
            suit = 'Wands'
          }
          if(titleOnCard.indexOf("Куб") + 1){
            suit = 'Cups'
          }
          if(titleOnCard.indexOf("Меч") + 1){
            suit = 'Swords'
          }
          if(titleOnCard.indexOf("Пентакл") + 1){
            suit = 'Pentacles'
          }
        }
        console.log("----------------------------------------> arcana " + arcana);

        console.log("----------------------------------------> suit " + suit);

          dataArr.push([{
            name: titleOnCard,
            text: textValue,
            meaning: meaningValue,
            reverse: reverseMeaningValue,
            arcana: arcana,
            suit: suit,
            hash: md4(sole + i),
            num: i
        }]);
      } catch (err) {
        console.error(err)
      }
      await delay(3000);
      i++;
    } else {
      await browser.close();
      console.log("dataArr", dataArr);
    }
  }

  for (let j = 0; j < dataArr.length; j++) {
    console.log("dataArr[j]",dataArr[j])
    //const insertResult = await collection.insertMany(dataArr[j]);
    console.log('Inserted documents =>', insertResult);
  }
}

getData()
  .then(console.log)
  .catch(console.error)
  .finally(() => client.close());



