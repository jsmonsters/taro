// Copyright IBM Corp. 2016,2019. All Rights Reserved.
// Node module: loopback-workspace
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

'use strict';

const loopback = require('loopback');
const boot = require('loopback-boot');
const fs = require('fs');
const express = require('express');
const multer = require('multer');
const {MongoClient} = require('mongodb');
const app = module.exports = loopback();
// eslint-disable-next-line max-len
const uploadsFolder = '/Users/Administrator/WebstormProjects/taro/lb/client/img/uploads';
const divinationsFolder = '/Users/Administrator/WebstormProjects/taro/lb/client/img/divinations';
const cardsFolder = '/Users/Administrator/WebstormProjects/taro/lb/client/img/cards';
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);
const dbName = 'taro';
const db = client.db(dbName);
//const collectionPage = db.collection('page');

(async () => {
  await client.connect();
})()

app.get('/delimg', function (req, res) {
  var check = false;
  var str = "";
  let imageNameInText = db.collection('page').find({'text': {'$regex': '.*' + req.query.img + '.*'}});
  imageNameInText.forEach(function (item) {
    console.log("item ", item);
    str += item.title + " ";
    check = true;
  }).then(result => {
    if (check) {
      res.send({"status": "This image cannot be deleted because it is using on the page(s): " + str});
    } else {
      fs.unlink(uploadsFolder + '/' + req.query.img, (err) => {
        if (err) throw err;
        console.log(req.query.img + ' was deleted');
      });
      res.send({"status": "The picture was deleted"});
    }
  }).then(result => console.log(result))
    .catch(err => console.error(err));
});


app.get(['/images.json', '/imagesDivination.json', '/imagesCard.json'], function (req, res) {
  console.log("req.url");
  console.log(req.url);
  let folder;
  if (req.url == '/images.json') {
    folder = uploadsFolder;
  }
  if (req.url == '/imagesDivination.json') {
    folder = divinationsFolder;
  }
  if (req.url == '/imagesCard.json') {
    folder = cardsFolder;
  }

  let str = '{"Images":[';
  fs.readdir(folder, (err, files) => {
    if (files.length) {
      files.forEach(file => {
        str += '{"name":"' + file + '"},';
      });
      str = str.slice(0, -1);
    }
    str += ']}';
    res.send(str);
  });
});


app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    const baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      const explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

app.options('/url...', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'POST');
  res.header('Access-Control-Allow-Headers', 'accept, content-type');
  res.header('Access-Control-Max-Age', '1728000');
  return res.sendStatus(200);
});

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});

app.use(express.static(__dirname));
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '../client/img/uploads');
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '_' + Date.now() + '.png');
  },
});
let upload = multer({storage: storage});

app.use(multer(upload).single('file'));

app.post('/upload', function (req, res, next) {
  console.log('---> upload');
  let file = req.file;
  console.log('--->' + file);
  if (!file)
    res.send('{"status":"fail"}');
  else
    res.send('{"status":"success"}');
});
