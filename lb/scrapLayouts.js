const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://astrohelper.ru/gadaniya/taro/');
  const pageNum = 5;
  let i = 1;

  await page.waitForSelector('.stretched-link')
  const linksArray = await page.$$('.stretched-link')

  for (let i in linksArray) {
    try {
      let linkOnLayout = await page.evaluate(el => el.textContent, linksArray[i]);
      console.log("linkOnLayout", linkOnLayout)
      let linkOnLayoutUrl = await page.evaluate(el => el.getAttribute('href'), linksArray[i]);
      console.log("linkOnLayoutUrl", linkOnLayoutUrl)
      if (linkOnLayoutUrl.indexOf("gadaniya/taro/") + 1) {
        linkOnLayoutUrl = linkOnLayoutUrl.replace('../../gadaniya/taro/', 'https://astrohelper.ru/gadaniya/taro/')
        console.log("linkOnLayoutUrl", linkOnLayoutUrl)
      } else {
        return;
      }

      const pageFromLink = await browser.newPage();
      await pageFromLink.goto(linkOnLayoutUrl);
      await pageFromLink.setViewport({width: 1440, height: 2080});
      await pageFromLink.screenshot({
        path: 'screenshots/' + i + '_layouts.png'
      });
      // todo : get text from page and insert the description in db

      await pageFromLink.waitForSelector('.lead')
      const manualArray = await pageFromLink.$$('blockquote');
      let manualText = await pageFromLink.evaluate(el => el.textContent, manualArray[0]);
      console.log("tips ---", manualText);

      const lead = await pageFromLink.$$('.lead');
      for (let c in lead) {
        let mainText = await pageFromLink.evaluate(el => el.textContent, lead[c]);
        console.log(i + " mainText #################################");
        console.log(mainText);
        console.log("/mainText #################################");
      }

      // delay(3000); blockquote
    } catch (e) {
      console.log('############ cant get page ############')
      console.log(e)
    }
  }

  await browser.close();

})();

process.on('unhandledRejection', function (err) {
  throw err;
});

process.on('uncaughtException', function (err) {
  throw err;
});
