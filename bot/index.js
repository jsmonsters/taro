const TelegramBot = require('node-telegram-bot-api');
var md4 = require('js-md4');
const {MongoClient} = require('mongodb');

/**
 *  first 78 the cards is meaning and next 78 cards the is inverted meaning
 * */
let token;

if (process.env.NODE_ENV == 'production') {
    console.log('env production');
    token = '5399107434:AAGVLXXhixvLkdNrjtkmeMlft2YLi9xJgVE';
} else {
    console.log('env dev');
    token = "2144098369:AAF3BnQHBRUVUVGBxm5NSONouaUExuXgiQk";
}

const bot = new TelegramBot(token, {polling: true});
var salt = "SomeText!"
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);
const dbName = 'taro';
const db = client.db(dbName);
const collectionCard = db.collection('cards');
const collectionLayout = db.collection("layout");
(async () => {
    await client.connect();
})()

async function delay(ms) {
    // return await for better async stack trace support in case of errors.
    return await new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Global vars
 * @type {*[]}
 */
let getLayoutArr = [];
let arcana = "Все";
let week = false;

/**
 * Function for provide access to var getLayoutArr
 * @param recordNum
 * @returns {Promise<(Document & {_id: InferIdType<Document>})|WithId<Document>[]|*[]|*>}
 */
async function getLayout(recordNum) {
    /*[
{
_id: new ObjectId("624d7f4bde40b834a0c84f40"),
layoutName: 'Гадание на ближайшее будущее',
layoutDescription: 'Таро расклад позволит узнать что ждет вас в будущем, сделать выводы и принять верное решение. В раскладе присутствуют все 78 карт, каждая из которых мо
жет дать ответ на вашу ситуацию в ближайшем будущем.',
cards: 8,
arcana: 'Все',
advice: 'Внимательно сосредоточьтесь на текущей или будущей неделе, а затем нажимайте на карты. Внизу будет представлена интерпретация вашего расклада.'
},
*/
    if (getLayoutArr.length) {
        if (!recordNum) {
            return getLayoutArr;
        } else {
            return getLayoutArr[+recordNum - 1]
        }
    }
//const sortByCard = await collectionLayout.find({}).sort({cards : 1}).toArray();
    if (!recordNum) {
        return await collectionLayout.find({}).sort({cards: 1}).toArray();
    } else {
        let record = await collectionLayout.find({}).sort({cards: 1}).toArray();
        return record[+recordNum - 1];
    }
}

/**
 *
 * @param cardNum number between 0 and 77 (taro pack has 78 cards)
 * @param opts object for storage additional params
 * @returns {Promise<void>}
 */
async function getCardByNum(cardNum, opts) {
    const filteredDocs = await collectionCard.find({num: cardNum}).toArray();
    var photo = "../lb/client/img/cards/" + opts.hashImg + ".png";
    var reversePhoto = "../lb/client/img/reverse/" + opts.hashImg + ".png";
    if (typeof filteredDocs[0] != "undefined" && filteredDocs[0].text.length > 1000) {
        filteredDocs[0].text = filteredDocs[0].text.substr(0, 1000) + "...";
    }
    let dayOfWeek = "";
    const dayOfWeekArr = ["Понедельник:", "Вторник:", "Среда:", "Четверг:", "Пятница:", "Суббота:", "Воскресенье:"];
    if (week) {
        dayOfWeek = dayOfWeekArr[opts.i];
    }
    await bot.sendPhoto(opts.chat_id, opts.cardNumReverse % 2 ? reversePhoto : photo, {
        caption: opts.cardNumReverse % 2
            ? dayOfWeek + `\nBаша карта перевёрнута \n` + filteredDocs[0].text
            : dayOfWeek + "\n" + filteredDocs[0].text
    });
    await bot.sendMessage(opts.chat_id, opts.cardNumReverse % 2
        ? dayOfWeek + '\nЗначение: \n' + filteredDocs[0].reverse
        : dayOfWeek + '\nЗначение: \n' + filteredDocs[0].meaning)
}

/**
 * First screen of the cards layout
 * @param callbackQuery
 */
function stepOne(callbackQuery) {
    let cbDataArr = callbackQuery.data.split("|||");
    const layoutName = cbDataArr[0];
    const action = cbDataArr[1];
    const msg = callbackQuery.message;
    const opts = {
        chat_id: msg.chat.id,
        message_id: msg.message_id,
    };
    (async () => {
        let layoutInfo;
        layoutInfo = await getLayout(action);
        await bot.sendMessage(opts.chat_id, "Расклад: \n" + layoutInfo.layoutName)
        await bot.sendMessage(opts.chat_id, "Совет: \n" + layoutInfo.advice)
        await bot.sendMessage(opts.chat_id, "Описание: \n" + layoutInfo.layoutDescription)
        await moreInfoBtn(opts, layoutInfo.cards);
    })()
}

/**
 *
 * @param callbackQuery
 * @param cards number of cards
 */
async function stepTwo(callbackQuery, cards) {
    console.log('********************************************')
    console.log(arcana);
    console.log('********************************************')

    if (cards == 7) {
        week = true;
    } else {
        week = false;
    }

    for (let i = 0; i < cards; i++) {
        const msg = callbackQuery.message;
        let cardNum = Math.floor(Math.random() * 78);
        console.log("arcana", arcana)
        if (arcana === "Старший") {
            cardNum = Math.floor(Math.random() * 22);
        }
        let cardNumReverse = Math.floor(Math.random() * 10);
        var hashImg = md4(salt + cardNum);
        const opts = {
            chat_id: msg.chat.id,
            message_id: msg.message_id,
            hashImg: hashImg,
            cardNumReverse: cardNumReverse,
            i: i,
            cards: cards,
        };
        console.log(i, cardNum)
        getCardByNum(cardNum, opts);
        await delay(1700)
    }
    return;
}

/**
 * Show button in first screen (func stepOne)
 * @param opts object with additional params (chat_id..)
 * @param cards number
 * @returns {Promise<void>}
 */
async function moreInfoBtn(opts, cards) {
    const chatId = opts.chat_id;
    var options = {
        reply_markup: JSON.stringify({
            inline_keyboard: [[{
                text: "Получить карт" + (cards === 1 ? 'у' : 'ы'),
                callback_data: "getCardAction|||" + cards
            }]]
        })
    };

    bot.sendMessage(chatId, "Нажмите на кнопку ниже, чтобы получить карт" + (cards === 1 ? 'у' : 'ы'), options);
}

/**
 *  inteface of telegram bot
 */
const start = () => {
    bot.setMyCommands([
        {command: '/start', description: 'Приветствие'},
    ])

    /**
     *  event listener for all buttons
     */
    bot.on('callback_query', function onCallbackQuery(callbackQuery) {
        if (callbackQuery.data.indexOf("getCardAction") !== -1) {
            const callbackArr = callbackQuery.data.split("|||")
            console.log("callbackQuery.data", callbackQuery.data)
            stepTwo(callbackQuery, callbackArr[1]);
        } else {
            console.log("---------------!!!!--------------")
            console.log(callbackQuery.data.charAt(0))
            if (callbackQuery.data.charAt(0) == 1) {
                arcana = "Старший";
            } else {
                arcana = "Все";
            }
            console.log("---------------!!!!--------------")
            stepOne(callbackQuery);
        }
    });


// Listen for any kind of message. There are different kinds of
// messages.
    bot.on('message', async msg => {
        const text = msg.text;
        const chatId = msg.chat.id;
        const buttonsArr = await getLayout(false);
        // send a message to the chat acknowledging receipt of their message
        if (text === "/start") {
            let buttons = [];
            let button;
            let buttonData;
            let arc = 0; // local var arcan;
            for (let i in buttonsArr) {
                console.log("buttonsArr[i].arcana", buttonsArr[i].arcana);
                console.log("buttonsArr[i].cards", buttonsArr[i].cards);
                if (buttonsArr[i].arcana === "Старший") {
                    arc = 1;
                }
                buttonData = arc + buttonsArr[i].layoutName + '|||' + (+i + 1)
                button = [{
                    text: buttonsArr[i].layoutName + ' (карт: ' + buttonsArr[i].cards + ', аркан: ' + buttonsArr[i].arcana + ')',
                    callback_data: buttonData
                }];
                buttons.push(button);
            }
            var options = {
                reply_markup: JSON.stringify({
                    inline_keyboard: buttons
                })
            };
            bot.sendMessage(chatId, "Выберите расклад", options);
        }
    });
}

start();

process.on('unhandledRejection', function (err) {
    throw err;
});

process.on('uncaughtException', function (err) {
    throw err;
});