var config = {};
config.category = ["children", "girls", "lovers", "dancers", "singers", "old people", "small people"];
config.colors = ["yellow", "blue", "yellow"];
config.colorsRandom = ["darkred", "violet", "green", "red", "magenta", "gray", "yellow", "blue", "purple", "darkslategrey", "brown", "rosybrown", "mediumslateblue", "bisque", "peru", "darkcyan"];
config.users = [{
    name: "Alyx",
    pseudo: "user1",
    role: "user",
    categories: ["old people", "small people"]
},{
    name: "Sofia",
    pseudo: "user2",
    role: "admin",
    categories: ["children", "girls"]
},{
    name: "User",
    pseudo: "user2",
    role: "admin",
    categories: ["children", "girls", "lovers", "dancers",]
}]