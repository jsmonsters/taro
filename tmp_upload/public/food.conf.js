var config = {};
config.category = ["fruits", "potato", "meat", "milk", "fish", "bread"];
config.colors = ["grey", "blue"];
config.colorsRandom = ["darkred", "violet", "green", "red", "magenta", "gray", "yellow", "blue", "purple", "darkslategrey", "brown", "rosybrown", "mediumslateblue", "bisque", "peru", "darkcyan"];
config.users = [{
    name: "Alex",
    pseudo: "user1",
    role: "user",
    categories: ["milk", "fish", "bread"]
},{
    name: "Sofia",
    pseudo: "user2",
    role: "admin",
    categories: ["fruits", "potato", "meat"]
}]