class jsonToPic {
    constructor(dataImages) {
        this.dataImages = dataImages;
    }

    getJSON(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.onload = function () {
            var status = xhr.status;
            if (status === 200) {
                callback(null, xhr.response);
            } else {
                callback(status, xhr.response);
            }
        };
        xhr.send();
    };

    getDataImg(el, folder) {
        let arr = [];
        this.dataImages = el.Images;

        if (!this.dataImages.length) {
            arr.push("https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png")
        } else {
            for (let i = 0; i < this.dataImages.length; i++) {
                console.log(this.dataImages[i].name);
                arr.push(folder + this.dataImages[i].name)
            }
        }
        return arr;
    }
}

